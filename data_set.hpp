#ifndef DATA_SET_HPP
#define DATA_SET_HPP

#include <cstdint>

struct DataSet_t
{
    uint32_t voltage;   /**< Voltage in [mV] */
    uint32_t current;   /**< Current in [mA] */
};

#endif // DATA_SET_HPP
