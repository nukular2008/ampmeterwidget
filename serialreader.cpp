#include "serialreader.h"

#include <QDebug>
#include <QSerialPortInfo>

#include "libs/cobs.h"

SerialReader::SerialReader(QObject *parent)
    : QObject (parent)
    , m_dataBuffer()
    , m_dataBufferIdx(0)
{

    connect(&m_pollPortsTimer, &QTimer::timeout, this, &SerialReader::onPollPortsTimerElapsed);
    connect(&m_serialPort, &QSerialPort::readyRead, this, &SerialReader::handleReadyRead);
    connect(&m_serialPort, &QSerialPort::errorOccurred, this, &SerialReader::handleError);

    Q_ASSERT(m_serialPort.setBaudRate(PORT_BAUD_RATE));

    m_pollPortsTimer.start(POLL_PORTS_INTERVAL);
}


/*----------------------------------------------------------------------------*/

void SerialReader::onPollPortsTimerElapsed()
{
    if (m_serialPort.isOpen())
    {
        return;
    }

    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
    {
        if (DEVICE_VENDOR_ID == info.vendorIdentifier()
                &&
            DEVICE_PRODUCT_ID == info.productIdentifier())
        {
            qDebug() << "AmpMeter connected (" << info.systemLocation() << ")";
            m_serialPort.setPortName(info.systemLocation());

            m_serialPort.clearError();
            const bool openSuccess = m_serialPort.open(QIODevice::ReadOnly);

            if (false == openSuccess)
            {
                qDebug() << "Could not open port";
            }

//            m_pollPortsTimer.stop();
        }
    }
}

/*----------------------------------------------------------------------------*/

void SerialReader::handleReadyRead()
{
    const qint64 bytesAvailable = m_serialPort.bytesAvailable();

    for (qint64 i = 0; i < bytesAvailable; i++)
    {
        char c;
        const qint64 bytesRead = m_serialPort.read(&c, 1);

        if (1 != bytesRead)
        {
            qDebug() << "Read incorrect number of bytes:" << bytesRead;
        }
        else
        {
            m_dataBuffer[m_dataBufferIdx] = c;
        }

        /* Check if we read a COBS termination character */
        if (0 == c)
        {
            processRxData();
            m_dataBufferIdx = 0;

        }
        else
        {
            m_dataBufferIdx += bytesRead;
            if (sizeof(m_dataBuffer) <= static_cast<size_t>(m_dataBufferIdx))
            {
                m_dataBufferIdx = 0;
            }
        }
    }
}

/*----------------------------------------------------------------------------*/

void SerialReader::handleError(QSerialPort::SerialPortError error)
{
    qDebug() << error;

    if (QSerialPort::SerialPortError::ResourceError == error)
    {
        qDebug() << "is a ressourc error!";
        emit(meterDisconnected());
    }

    if (m_serialPort.isOpen())
    {
        m_serialPort.close();
        m_serialPort.clearError();
    }

}

/*----------------------------------------------------------------------------*/

void SerialReader::handleClose()
{
    qDebug() << "Serial port closing";

//    if (m_serialPort.isOpen())
//    {
//        m_serialPort.close();
//    }
}

/*----------------------------------------------------------------------------*/

void SerialReader::processRxData()
{
    /* COBS DECODE received data */
    uint8_t decodedData[sizeof(m_dataBuffer)];

    const cobs_decode_result decodeResult = cobs_decode(decodedData, sizeof(decodedData), m_dataBuffer, m_dataBufferIdx);

    if (COBS_DECODE_OK != decodeResult.status)
    {
        qDebug() << "COBS decode failed:" << decodeResult.status;
        return;
    }

    DataSet_t dataSet;

    memcpy(&dataSet, decodedData, sizeof(dataSet));

    qDebug() << "Voltage:" << dataSet.voltage << "mV | Current:" << dataSet.current << "mA";

    emit(dataReceived(dataSet));
}
