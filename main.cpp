#include "mainwindow.h"
#include <QApplication>

#include "serialreader.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    SerialReader reader;

    MainWindow w(reader);
    w.show();

    return a.exec();
}
