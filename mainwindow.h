#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "data_set.hpp"
#include "serialreader.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(SerialReader &reader, QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void updateValues(DataSet_t data);
    void onDisconnect();

private:
    Ui::MainWindow *ui;

    SerialReader &m_reader;
};

#endif // MAINWINDOW_H
