#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

#include "serialreader.h"

MainWindow::MainWindow(SerialReader &reader, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_reader(reader)
{
    connect(&m_reader, &SerialReader::dataReceived, this, &MainWindow::updateValues);
    connect(&m_reader, &SerialReader::meterDisconnected, this, &MainWindow::onDisconnect);

//    setAttribute(Qt::WA_TranslucentBackground);
    setWindowFlags(Qt::Widget |Qt::FramelessWindowHint);
    setAttribute(Qt::WA_NoSystemBackground, true);
    setAttribute(Qt::WA_TranslucentBackground, true);

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*----------------------------------------------------------------------------*/

void MainWindow::updateValues(DataSet_t data)
{
    ui->labelNotConnected->setVisible(false);
    const uint32_t volt      = data.voltage / 1000;
    const uint32_t milliVolt = data.voltage % 1000;

    const uint32_t amps      = data.current / 1000;
    const uint32_t milliAmps = data.current % 1000;

//    QString voltStr = QString("%1mV").arg(data.voltage);
//    QString ampStr = QString("%1mA").arg(data.current);

    QString voltStr;
    voltStr.sprintf("%2u.%03uV", volt, milliVolt);

    QString ampStr;
    ampStr.sprintf("%2u.%03uA", amps, milliAmps);

    ui->labelVolt->setText(voltStr);
    ui->labelCurrent->setText(ampStr);
}

/*----------------------------------------------------------------------------*/

void MainWindow::onDisconnect()
{
    qDebug() << "onDisconnect";

    ui->labelNotConnected->setVisible(true);
    QString voltStr;
    voltStr.sprintf("-----");

    QString ampStr;
    ampStr.sprintf("-----"
                   "");

    ui->labelVolt->setText(voltStr);
    ui->labelCurrent->setText(ampStr);

}

/*----------------------------------------------------------------------------*/
