#ifndef SERIALREADER_H
#define SERIALREADER_H

#include <cstdint>

#include <QObject>
#include <QByteArray>
#include <QSerialPort>
#include <QTextStream>
#include <QTimer>

#include "data_set.hpp"



class SerialReader : public QObject
{
    Q_OBJECT

public:
    explicit SerialReader(QObject *parent = nullptr);

Q_SIGNALS:
    void dataReceived(DataSet_t data);
    void meterDisconnected();

private slots:
    void handleReadyRead();
    void handleError(QSerialPort::SerialPortError error);
    void handleClose();

    void onPollPortsTimerElapsed();

private:

    /* private constants                                                      */
    static const uint32_t   POLL_PORTS_INTERVAL   = 100;      /**< Interval in which COM ports will be polled to see if a
                                                                   correct device was connected */
    static const uint16_t   DEVICE_VENDOR_ID      = 0x0483;
    static const uint16_t   DEVICE_PRODUCT_ID     = 0x374B;

    static const QSerialPort::BaudRate PORT_BAUD_RATE = QSerialPort::Baud115200;

    static const size_t DATA_BUFFER_SIZE = sizeof (DataSet_t) + 2;

    /* private methods                                                        */
    void processRxData();

    /* private variables                                                      */
    QSerialPort m_serialPort;
    QByteArray m_readData;
    QTimer m_pollPortsTimer;

    char m_dataBuffer[DATA_BUFFER_SIZE];
    qint64 m_dataBufferIdx;
};

#endif // SERIALREADER_H
